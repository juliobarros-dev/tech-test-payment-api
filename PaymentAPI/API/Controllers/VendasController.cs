﻿using Microsoft.AspNetCore.Mvc;
using Models.Enums;
using Models.Models;

namespace API.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class VendasController : ControllerBase
  {

    [HttpPost]
    public IActionResult CriarVenda(Venda venda)
    {
      List<Venda> mockedVendaTable = new List<Venda>();
      mockedVendaTable.Add(venda);

      return Ok(venda);
    }

    [HttpGet("{id}")]
    public IActionResult VendaById(int id)
    {
      List<Venda> mockedVendaTable = new List<Venda>();
      mockedVendaTable.Add(new Venda() { Id = 1, Status = VendaStatus.Aguardando_pagamento, Vendedor = new Vendedor() { Cpf = "9595959595", Email = "johndoe@email", Id = 1, Nome = "John Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 1, Nome = "Loren Ypsun", Preco = 5 } } });
      mockedVendaTable.Add(new Venda() { Id = 2, Status = VendaStatus.Cancelada, Vendedor = new Vendedor() { Cpf = "6161651665", Email = "marydoe@email", Id = 2, Nome = "Mary Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 2, Nome = "Ypsyn Ypsun", Preco = 7 } } });
      mockedVendaTable.Add(new Venda() { Id = 3, Status = VendaStatus.Pagamento_aprovado, Vendedor = new Vendedor() { Cpf = "1651651651", Email = "barbiedoe@email", Id = 3, Nome = "Barbie Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 3, Nome = "Ypsun Loren", Preco = 8 } } });
      mockedVendaTable.Add(new Venda() { Id = 4, Status = VendaStatus.Enviado_para_Transportadora, Vendedor = new Vendedor() { Cpf = "8481778181", Email = "ken@email", Id = 4, Nome = "Ken Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 4, Nome = "Lops Ylson", Preco = 9 } } });


      var result = mockedVendaTable.FirstOrDefault(x => x.Id == id);

      if (result is null) return NotFound();

      return Ok(result);
    }

    [HttpPut("{id}")]
    public IActionResult UpdateStatus(int id, VendaStatus status)
    {
      List<Venda> mockedVendaTable = new List<Venda>();
      mockedVendaTable.Add(new Venda() { Id = 1, Status = VendaStatus.Aguardando_pagamento, Vendedor = new Vendedor() { Cpf = "9595959595", Email = "johndoe@email", Id = 1, Nome = "John Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 1, Nome = "Loren Ypsun", Preco = 5 } } });
      mockedVendaTable.Add(new Venda() { Id = 2, Status = VendaStatus.Cancelada, Vendedor = new Vendedor() { Cpf = "6161651665", Email = "marydoe@email", Id = 2, Nome = "Mary Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 2, Nome = "Ypsyn Ypsun", Preco = 7 } } });
      mockedVendaTable.Add(new Venda() { Id = 3, Status = VendaStatus.Pagamento_aprovado, Vendedor = new Vendedor() { Cpf = "1651651651", Email = "barbiedoe@email", Id = 3, Nome = "Barbie Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 3, Nome = "Ypsun Loren", Preco = 8 } } });
      mockedVendaTable.Add(new Venda() { Id = 4, Status = VendaStatus.Enviado_para_Transportadora, Vendedor = new Vendedor() { Cpf = "8481778181", Email = "ken@email", Id = 4, Nome = "Ken Doe", Telefone = "959599191" }, Produtos = new List<Produto>() { new Produto() { Id = 4, Nome = "Lops Ylson", Preco = 9 } } });


      var result = mockedVendaTable.FirstOrDefault(x => x.Id == id);

      if (result is null) return NotFound();

      var response = result.AtualizarStatus(status);

      if (response.Any()) return BadRequest(response[0]);

      return Ok(result);
    }
  }
}
