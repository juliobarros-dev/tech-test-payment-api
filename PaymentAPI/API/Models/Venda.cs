﻿using Models.Enums;

namespace Models.Models
{
  public class Venda
  {
    public int Id { get; set; }
    public Vendedor Vendedor { get; set; } = new Vendedor();
    public DateTime DataDaVenda { get; set; }
    public List<Produto> Produtos { get; set; } = new List<Produto>();
    public VendaStatus Status { get; set; }

    public Venda()
    {
    }

    public Venda(Vendedor vendedor, List<Produto> produtos)
    {
      Vendedor = vendedor;
      Produtos = produtos;
      Status = VendaStatus.Aguardando_pagamento;
    }

    public List<string> AtualizarStatus(VendaStatus status)
    {
      List<string> errorMessage = new List<string>();

      if ((Status == VendaStatus.Aguardando_pagamento && status == VendaStatus.Pagamento_aprovado) || (Status == VendaStatus.Aguardando_pagamento && status == VendaStatus.Cancelada))
      {
        Status = status;
      }
      else if ((Status == VendaStatus.Pagamento_aprovado && status == VendaStatus.Enviado_para_Transportadora) || (Status == VendaStatus.Pagamento_aprovado && status == VendaStatus.Cancelada))
      {
        Status = status;
      }
      else if (Status == VendaStatus.Enviado_para_Transportadora && status == VendaStatus.Entregue)
      {
        Status = status;
      }
      else
      {
        errorMessage.Add("Status inválido");
      }

      return errorMessage;
    }
  }
}
