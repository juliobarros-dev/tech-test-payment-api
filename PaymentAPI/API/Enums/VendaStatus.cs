﻿namespace Models.Enums
{
  public enum VendaStatus
  {
    Aguardando_pagamento,
    Pagamento_aprovado,
    Cancelada,
    Enviado_para_Transportadora,
    Entregue
  }
}
