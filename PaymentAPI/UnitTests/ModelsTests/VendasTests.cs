﻿using Models.Enums;
using Models.Models;

namespace UnitTests.ModelsTests
{
  public class VendasTests
  {

    [Fact]
    public void AtualizarStatusShouldReturnEmptyListGivenValidStatusForAguardando_pagamento()
    {
      Venda venda = new Venda() { Status = VendaStatus.Aguardando_pagamento };
      List<string> errorMessage = new List<string>();

      Assert.Equal(errorMessage, venda.AtualizarStatus(VendaStatus.Pagamento_aprovado));
      Assert.Equal(errorMessage, venda.AtualizarStatus(VendaStatus.Cancelada));
    }

    [Fact]
    public void AtualizarStatusShouldReturnEmptyListGivenValidStatusForPagamento_aprovado()
    {
      Venda venda = new Venda() { Status = VendaStatus.Pagamento_aprovado };
      List<string> errorMessage = new List<string>();

      Assert.Equal(errorMessage, venda.AtualizarStatus(VendaStatus.Enviado_para_Transportadora));
    }

    [Fact]
    public void AtualizarStatusShouldReturnEmptyListGivenValidStatusForEnviado_para_Transportadora()
    {
      Venda venda = new Venda() { Status = VendaStatus.Enviado_para_Transportadora };
      List<string> errorMessage = new List<string>();

      Assert.Equal(errorMessage, venda.AtualizarStatus(VendaStatus.Entregue));
    }

    [Fact]
    public void StatusShouldUpdateGivenCorrectStatusFlow()
    {
      Venda venda = new Venda() { Status = VendaStatus.Aguardando_pagamento };
      
      venda.AtualizarStatus(VendaStatus.Pagamento_aprovado);
      Assert.Equal(VendaStatus.Pagamento_aprovado, venda.Status);
      
      venda.AtualizarStatus(VendaStatus.Enviado_para_Transportadora);
      Assert.Equal(VendaStatus.Enviado_para_Transportadora, venda.Status);

      venda.AtualizarStatus(VendaStatus.Entregue);
      Assert.Equal(VendaStatus.Entregue, venda.Status);
    }

    [Fact]
    public void VendaShouldNotCancelAfterEnviado_para_Transportadora()
    {
      Venda venda = new Venda() { Status = VendaStatus.Enviado_para_Transportadora };

      List<string> errorMessage = new List<string>() { "Status inválido" };

      Assert.Equal(errorMessage, venda.AtualizarStatus(VendaStatus.Cancelada));
    }

    [Fact]
    public void VendaShouldBeCanceladaIfStatusIsAguardando_pagamento()
    {
      Venda venda = new Venda() { Status = VendaStatus.Aguardando_pagamento };

      venda.AtualizarStatus(VendaStatus.Cancelada);
      Assert.Equal(VendaStatus.Cancelada, venda.Status);
    }

    [Fact]
    public void VendaShouldBeCanceladaIfStatusIsPagamento_aprovado()
    {
      Venda venda = new Venda() { Status = VendaStatus.Pagamento_aprovado };

      venda.AtualizarStatus(VendaStatus.Cancelada);
      Assert.Equal(VendaStatus.Cancelada, venda.Status);
    }
  }
}
